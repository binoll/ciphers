# Ciphers (C)

## Overview
A program for encryption and decryption using various algorithms.

## Build the project
From the root of the repository, run following commands:

```bash
mkdir build && cd build
cmake ..
cmake --build .
```